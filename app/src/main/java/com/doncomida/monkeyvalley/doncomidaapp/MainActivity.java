package com.doncomida.monkeyvalley.doncomidaapp;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
//import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public int nTotal = 1;

    public ImageView btn_agregar;
    public ImageView btn_remover;
    public TextView lvl_total;
    public TextView lvl_cantidad;
    public TextView lvl_platillo;
    public TextView lvl_descripcion;
    public LinearLayout btn_pedir;

    public DTO_Login login;
    public final Gson gson = new Gson();

    private DatabaseReference mDatabase;

    public ImageView badge_comentarios;
    public ImageButton btn_comentarios;



    //bottom sheet
    public BottomSheetBehavior mBottomSheetBehavior;
    private View bottomSheet;
    public ImageButton btn_cerrarComentarios;
    public EditText ed_comentarios;


    public RelativeLayout container_cocina;
    public RelativeLayout container_comida;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        FirebaseMessaging.getInstance().subscribeToTopic("general")
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        String msg = "Bienvenido";
//                        if (!task.isSuccessful()) {
//                            msg = "Error al registrar";
//                        }
//                        Log.d(TAG, msg);
//                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
//                    }
//                });


        container_cocina = findViewById(R.id.container_cocina);
        container_comida = findViewById(R.id.container_comida);

        //Date currentTime = Calendar.getInstance().getTime();



        ObtenerEstatusCocina();



        String cDatosLogin = Helpers.getPrefe("DTO_Login","");
        login = gson.fromJson(cDatosLogin,DTO_Login.class);

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        day = day-1;
        Log.d(TAG, "Dia: " + (day) + "");


        ed_comentarios = findViewById(R.id.ed_comentarios);
        badge_comentarios = findViewById(R.id.badge_comentarios);

        //bottom sheet
        this.bottomSheet = findViewById(R.id.bottom_sheetAlerta);
        this.mBottomSheetBehavior = BottomSheetBehavior.from(this.bottomSheet);

        this.mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:

                        hideSoftKeyboard(MainActivity.this);

                        if(ed_comentarios.getText().toString().length() > 7)
                        {
                            badge_comentarios.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            badge_comentarios.setVisibility(View.GONE);

                        }


                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });






        btn_comentarios = findViewById(R.id.btn_comentarios);
        btn_comentarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //abrir bottomsheet
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        btn_cerrarComentarios = findViewById(R.id.btn_cerrarComentarios);
        btn_cerrarComentarios.setOnClickListener(v -> {

            //Cerrar bottomsheet
            hideSoftKeyboard(MainActivity.this);

            Handler handler = new Handler();
            handler.postDelayed(this::CerrarBottomShhet, 400);

        });

        lvl_platillo = findViewById(R.id.lvl_platillo);
        lvl_descripcion = findViewById(R.id.lvl_descripcion);
        lvl_cantidad = findViewById(R.id.lvl_cantidad);

        lvl_cantidad.setText(nTotal + "");
        lvl_total = findViewById(R.id.lvl_total);
        lvl_total.setText((nTotal * 50) + "");

        btn_agregar = findViewById(R.id.btn_agregar);
        btn_agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nTotal += 1;
                lvl_total.setText((nTotal * 50) + "");
                lvl_cantidad.setText(nTotal + "");

            }
        });
        btn_remover = findViewById(R.id.btn_remover);
        btn_remover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nTotal -= 1;
                nTotal = nTotal < 1 ? 1 : nTotal;
                lvl_total.setText((nTotal * 50) + "");
                lvl_cantidad.setText(nTotal + "");

            }
        });

        btn_pedir = findViewById(R.id.btn_pedir);
        btn_pedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValidarPedido();
            }
        });


        ObtenerPlatilloDelDia(day);


    }




    public void ObtenerPlatilloDelDia(int nDia)
    {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(nDia+ "");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                String[] lstPlatillo = value.split("\\|");
                Log.d(TAG, "Value is: " + value);
                lvl_platillo.setText(lstPlatillo[0]);
                lvl_descripcion.setText(lstPlatillo[1]);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public void ValidarPedido()
    {
        String cComentario = ed_comentarios.getText().toString().length() < 7 ? "" : ed_comentarios.getText().toString();
        final DTO_Pedido pedido = new DTO_Pedido(login.cNombre,login.cTelefono,nTotal,nTotal*50, cComentario);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("lstSucursales/" + login.cSucursal + "/lstPedidos/" + login.cTelefono);


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                final String value = gson.toJson(dataSnapshot.getValue());
                Log.d("ValidarPedido", "Value is: " + value.length());

                if(value.length() == 4)
                {

                    new AlertDialog.Builder(MainActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Confirmar pedido")
                            .setMessage("pedir " + nTotal + " platillos?")
                            .setPositiveButton("Si", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    HacerPedido(pedido,myRef);
                                }

                            })
                            .setNegativeButton("No", null)
                            .show();
                }
                else
                {

                    new AlertDialog.Builder(MainActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Confirmar pedido")
                            .setMessage("Ya tienes un pedido hecho, deseas agregar " + nTotal + " platillos?")
                            .setPositiveButton("Si", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ModificarPedido(value,pedido,myRef);
                                }

                            })
                            .setNegativeButton("No", null)
                            .show();
                }


            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("ValidarPedido", "Failed to read value.", error.toException());
            }
        });

        //myRef.setValue("Hello, World!");
    }


    public void HacerPedido(DTO_Pedido pedido,DatabaseReference myRef)
    {
        myRef.setValue(pedido);
        nTotal = 1;
        lvl_total.setText((nTotal * 50) + "");
        lvl_cantidad.setText(nTotal + "");
    }
    public void ModificarPedido(String cPedido, DTO_Pedido pedidoNuevo,DatabaseReference myRef)
    {
        DTO_Pedido pedido = gson.fromJson(cPedido,DTO_Pedido.class);
        pedido.nPlatillos = pedido.nPlatillos + pedidoNuevo.nPlatillos;
        pedido.nTotal = pedido.nPlatillos * 50;

        myRef.setValue(pedido);

        nTotal = 1;
        lvl_total.setText((nTotal * 50) + "");
        lvl_cantidad.setText(nTotal + "");
    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


    public void CerrarBottomShhet()
    {
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    public void ObtenerEstatusCocina()
    {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("bCocinaAbierta");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                boolean value = dataSnapshot.getValue(boolean.class);

                if(value)
                {
                    container_cocina.setVisibility(View.GONE);
                    container_comida.setVisibility(View.VISIBLE);
                }
                else
                {
                    container_cocina.setVisibility(View.VISIBLE);
                    container_comida.setVisibility(View.GONE);
                }
                Log.d(TAG, "ObtenerEstatusCocina is: " + value );

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }


}
