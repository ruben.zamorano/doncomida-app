package com.doncomida.monkeyvalley.doncomidaapp;

public class DTO_Pedido {
    public String cNombre;
    public String cTelefono;
    public int nPlatillos;
    public int nTotal;
    public String cComentarios;

    public DTO_Pedido(String cNombre, String cTelefono, int nPlatillos, int nTotal, String cComentarios) {
        this.cNombre = cNombre;
        this.cTelefono = cTelefono;
        this.nPlatillos = nPlatillos;
        this.nTotal = nTotal;
        this.cComentarios = cComentarios;
    }

    public String getcNombre() {
        return cNombre;
    }

    public void setcNombre(String cNombre) {
        this.cNombre = cNombre;
    }

    public String getcTelefono() {
        return cTelefono;
    }

    public void setcTelefono(String cTelefono) {
        this.cTelefono = cTelefono;
    }

    public int getnPlatillos() {
        return nPlatillos;
    }

    public void setnPlatillos(int nPlatillos) {
        this.nPlatillos = nPlatillos;
    }

    public int getnTotal() {
        return nTotal;
    }

    public void setnTotal(int nTotal) {
        this.nTotal = nTotal;
    }

    public String getcComentarios() {
        return cComentarios;
    }

    public void setcComentarios(String cComentarios) {
        this.cComentarios = cComentarios;
    }
}
