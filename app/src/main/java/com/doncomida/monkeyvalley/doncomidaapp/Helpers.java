package com.doncomida.monkeyvalley.doncomidaapp;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by rene on 22/02/18.
 */

public class Helpers {
    private static String codePattern = "^[0-9]{1,4}$";
    private static String numbersPattern = "\\d+";

    private static Integer passwordLenth = 4;

    private static ProgressDialog dialog;

    private static String datePattern = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)(?:0?2|(?:Feb))\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";

    private static Context appContext;

    public static String str01800 = "";

    public static void setContext(Context context) {
        appContext = context;
    }


    public static float dpToPx(float valueInDp) {
        DisplayMetrics metrics = appContext.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public static void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = appContext.getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) appContext.getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        appContext.getResources().updateConfiguration(configuration, metrics);
    }

    public static String getDiccionarioField(String field) {
        String diccionario = getPrefe("diccionarioFields");

        if (!diccionario.equals("0")) {
            try {
                JSONObject diccionarioFields = new JSONObject(diccionario);

                if (diccionarioFields.has(field)) {
                    return diccionarioFields.getString(field);
                } else {
                    return null;
                }

            } catch (JSONException e) {
                Log.i("JSONException", e.getMessage());
                return null;
            }
        } else {
            return null;
        }
    }

    public static JSONObject getDiccionarioActivity(String nameActivity) {
        String diccionario = getPrefe("diccionario");
        if (!diccionario.equals("0")) {
            try {
                JSONObject diccionarioInicioSeion = null;
                JSONArray diccionarioPantallas = new JSONArray(diccionario);
                for (int i = 0; i < diccionarioPantallas.length(); i++) {
                    JSONObject pantalla = diccionarioPantallas.getJSONObject(i);
                    if (pantalla.has(nameActivity)) {
                        diccionarioInicioSeion = pantalla.getJSONObject(nameActivity);
                        break;
                    }
                }
                return diccionarioInicioSeion;
            } catch (JSONException e) {
                Log.i("JSONException", e.getMessage());
                return null;
            }
        } else {
            return null;
        }
    }

    public static JSONArray getDiccionarioActivityArray(String nameActivity) {
        String diccionario = getPrefe("diccionario");
        if (!diccionario.equals("0")) {
            try {
                JSONArray diccionarioInicioSeion = null;
                JSONArray diccionarioPantallas = new JSONArray(diccionario);
                for (int i = 0; i < diccionarioPantallas.length(); i++) {
                    JSONObject pantalla = diccionarioPantallas.getJSONObject(i);
                    if (pantalla.has(nameActivity)) {
                        diccionarioInicioSeion = pantalla.getJSONArray(nameActivity);
                        break;
                    }
                }
                return diccionarioInicioSeion;
            } catch (JSONException e) {
                Log.i("JSONException", e.getMessage());
                return null;
            }
        } else {
            return null;
        }
    }



    public static boolean isEmpty(String value) {
        String valueTrim = value.trim();
        return valueTrim.equals("");
    }

    public static boolean isValidDate(String value) {
        Pattern pattern = Pattern.compile(datePattern);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }

    public static boolean isValidEmail(@NonNull String value) {
        return !value.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches();
    }

    public static boolean isValidCellPhone(String value) {
        Pattern pattern = Pattern.compile(numbersPattern);
        return pattern.matcher(value).matches();
    }

    public static boolean isValidPassLogin(String value) {
        return value.length() >= passwordLenth;
    }

    public static void toggleProgress(ProgressBar progressBar, final Activity activity) {
        if (progressBar.getVisibility() == View.INVISIBLE || progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }, 5000);
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            progressBar.setVisibility(View.GONE);
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public static void ocultarProgress(ProgressBar progressBar, final Activity activity) {
        if (progressBar.getVisibility() == View.VISIBLE ) {
            progressBar.setVisibility(View.GONE);
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public static void showLoader(Context context, String mensaje) {
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(mensaje);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void hideLoader() {
        if (dialog != null && dialog.isShowing()) {
            dialog.hide();
        }
    }

    public static void cleanPrefe(String key) {
        SharedPreferences prefe = appContext.getSharedPreferences("datos", Context.MODE_PRIVATE);
        prefe.edit().remove(key).apply();
    }

    public static void cleanAllPrefe() {
        appContext.getSharedPreferences("datos", 0).edit().clear().apply();
    }

    public static String getPrefe(String key) {
        String defaultVal = "";
        SharedPreferences prefe = appContext.getSharedPreferences("datos", Context.MODE_PRIVATE);
        if (prefe != null) {
            return prefe.getString(key, defaultVal);
        }
        else {
            return defaultVal;
        }
    }

    public static String getPrefe(String key, String defaultVal) {
        SharedPreferences prefe = appContext.getSharedPreferences("datos", Context.MODE_PRIVATE);
        return prefe.getString(key, defaultVal);
    }

    public static void setPrefe(String key, String value) {
        SharedPreferences prefe = appContext.getSharedPreferences("datos", Context.MODE_PRIVATE);
        prefe.edit().putString(key, value).apply();
    }

    public static void setPrefeBool(String key, Boolean value) {
        SharedPreferences prefe = appContext.getSharedPreferences("datos", Context.MODE_PRIVATE);
        prefe.edit().putBoolean(key, value).apply();
    }

    public static Boolean getPrefeBool(String key, Boolean defaultVal) {
        SharedPreferences prefe = appContext.getSharedPreferences("datos", Context.MODE_PRIVATE);
        return prefe.getBoolean(key, defaultVal);
    }

    public static String cleanText(String input, boolean upper) {
        input = upper ? input.trim().toUpperCase() : input.trim().toLowerCase();
        input = Helpers.removeAccents(input);
        return input;
    }

    public static String removeAccents (String input) {
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ, ";
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC +";
        String output = input;
        for (int i=0; i<original.length(); i++) {
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }
        return output;
    }

    public static String log(String pre, String logMsg) {
        return pre + ": " + logMsg;
    }

    public static void closeKeyboard(View view, Context context, Boolean isShowKeyboard) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        else {
            if (isShowKeyboard) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        }
    }

    public static String dateFormated(String format, String formatToConvert, String dateString) {
        try {
            Date date = new SimpleDateFormat(format, Locale.US).parse(dateString);
            return new SimpleDateFormat(formatToConvert, Locale.US).format(date);
        }
        catch (ParseException e ){
            return null;
        }
    }




    public static void changeColorStatusBar(Activity activity, int colorV23, int colorV21) {
        Window window = activity.getWindow();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.setStatusBarColor(colorV23);
        }
        else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(activity.getResources().getColor(colorV21));
        }
    }

    public static String formatNumber(String number) {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
        return format.format(Float.parseFloat(number)).replace(".00","");
    }



    public static View getToolbarTitle(Context context, Toolbar toolbar) {
        int childCount = toolbar.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = toolbar.getChildAt(i);
            if (child instanceof TextView) {
                return child;
            }
        }
        return new View(context);
    }

    public static String getSexo(String sexo)
    {
        if(sexo.equals("Female"))
        {
            return "Femenino";
        }
        else if(sexo.equals("Male"))
        {
            return "Masculino";
        }
        else
        {
            return "";
        }
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
