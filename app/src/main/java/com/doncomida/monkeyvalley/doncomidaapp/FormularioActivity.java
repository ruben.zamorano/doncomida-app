package com.doncomida.monkeyvalley.doncomidaapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.vicmikhailau.maskededittext.MaskedEditText;

import java.util.ArrayList;
import java.util.List;

public class FormularioActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public final Gson gson = new Gson();
    String cSucursal = "";
    public MaskedEditText ed_numerotelefono;
    public EditText ed_nombrecompleto;
    public LinearLayout btn_registrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Helpers.setContext(this);
        setContentView(R.layout.activity_formulario);

        String cDatosLogin = Helpers.getPrefe("DTO_Login","");
        if(cDatosLogin.length() > 0)
        {
            Intent instent = new Intent(this,MainActivity.class);
            startActivity(instent);
            finish();

            return;
        }


        ed_numerotelefono = findViewById(R.id.ed_numerotelefono);
        ed_nombrecompleto = findViewById(R.id.ed_nombrecompleto);

        btn_registrar = findViewById(R.id.btn_registrar);
        btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValidarRegistro();
            }
        });


        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Primavera");
        categories.add("Corporativo");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

    }

    public void ValidarRegistro()
    {
        boolean bError = false;

        if(ed_numerotelefono.getText().toString().replace("-","").trim().length() < 10)
        {
            bError = true;
            ed_numerotelefono.setError("Dato obligatorio");
        }else ed_numerotelefono.setError(null);

        if(ed_nombrecompleto.getText().toString().length() < 3)
        {
            bError = true;
            ed_nombrecompleto.setError("Dato obligatorio");
        }else ed_nombrecompleto.setError(null);


        if(!bError)
        {
            GuardarDatos();
        }
    }

    public void GuardarDatos()
    {
        DTO_Login login = new DTO_Login(ed_nombrecompleto.getText().toString(),ed_numerotelefono.getText().toString().replace("-","").trim(),cSucursal);
        Helpers.setPrefe("DTO_Login",gson.toJson(login));

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("lstSucursales/" + cSucursal + "/lstUsuarios/" + login.cTelefono);

        myRef.setValue(login);

        Intent instent = new Intent(this,MainActivity.class);
        startActivity(instent);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        cSucursal = item;

        // Showing selected spinner item
//        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }




}
