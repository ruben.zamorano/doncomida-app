package com.doncomida.monkeyvalley.doncomidaapp;

public class DTO_Login {
    public String cNombre;
    public String cTelefono;
    public String cSucursal;

    public DTO_Login(String cNombre, String cTelefono, String cSucursal) {
        this.cNombre = cNombre;
        this.cTelefono = cTelefono;
        this.cSucursal = cSucursal;
    }

    public String getcNombre() {
        return cNombre;
    }

    public void setcNombre(String cNombre) {
        this.cNombre = cNombre;
    }

    public String getcTelefono() {
        return cTelefono;
    }

    public void setcTelefono(String cTelefono) {
        this.cTelefono = cTelefono;
    }

    public String getcSucursal() {
        return cSucursal;
    }

    public void setcSucursal(String cSucursal) {
        this.cSucursal = cSucursal;
    }
}
